import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    messages: []
  },
  mutations: {
    updateMessage (state, [message, url]) {
        console.log(message)
        state.messages.push(message);
        const paramDict = {
            "@type": "Message",
            "text": message
        };
        const config = {
            headers:{
               'X-Requested-With': 'XMLHttpRequest',
                'Content-Type':'application/json',
                'Authorization': 'Basic cm9vdDpyb290'
            },
            // withCredentials:true,
          };
          axios.post(url, paramDict, config)
              .then(function(res){
                  console.log(res);
              })
              .catch(function(res){
                  console.log(res);
                  alert("Server connection error")
              });
    },
    initMessages (state, url) {
      let messages = [];
      const config = {
            headers:{
              'X-Requested-With': 'XMLHttpRequest',
              'Content-Type':'application/json',
              'Authorization': 'Basic cm9vdDpyb290'
            },
            // withCredentials:true,
          };
      axios.get(url, config)
          .then(function(res){
              const items = res.data;
              if (items !== undefined) {
                  for (let item of items) {
                      messages.push(item['text'])
                  }
              }
          })
          .catch(function(res){
            console.log(res);
            alert("Server connection error")
          })
      state.messages = messages
    }
  },
  actions: {

  }
})


