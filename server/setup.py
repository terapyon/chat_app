from setuptools import find_packages
from setuptools import setup


try:
    README = open('README.rst').read()
except IOError:
    README = None

setup(
    name='chat_app',
    version="1.0.0",
    description='Chat APP',
    long_description=README,
    install_requires=[
        'guillotina',
        'guillotina_swagger',
        'guillotina_pgcatalog',
        'guillotina_dbusers',
    ],
    author='Manabu TERADA',
    author_email='terada@cmscom.jp',
    url='',
    packages=find_packages(exclude=['demo']),
    include_package_data=True,
    tests_require=[
        'pytest',
    ],
    extras_require={
        'test': [
            'pytest'
        ]
    },
    classifiers=[],
    entry_points={
    }
)
