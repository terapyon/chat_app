from guillotina import configure
from guillotina.component import getMultiAdapter
from guillotina.interfaces import IContainer, IResourceSerializeToJsonSummary
from guillotina.utils import get_authenticated_user_id
from .content import IChanel


@configure.service(for_=IChanel, name='@messages',
                   permission='guillotina.Authenticated')
async def get_conversations(context, request):
    results = []
    async for message in context.async_values():
        summary = await getMultiAdapter(
            (message, request),
            IResourceSerializeToJsonSummary)()
        results.append(summary)
    results = sorted(results, key=lambda mes: mes['creation_date'])
    return results

