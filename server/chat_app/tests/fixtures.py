from guillotina import testing
from guillotina.tests.fixtures import ContainerRequesterAsyncContextManager

import json
import pytest


def base_settings_configurator(settings):
    if 'applications' in settings:
        settings['applications'].append('chat_app')
    else:
        settings['applications'] = ['chat_app']


testing.configure_with(base_settings_configurator)


class chat_app_Requester(ContainerRequesterAsyncContextManager):  # noqa

    async def __aenter__(self):
        await super().__aenter__()
        resp = await self.requester(
            'POST', '/db/guillotina/@addons',
            data=json.dumps({
                'id': 'chat_app'
            })
        )
        return self.requester


@pytest.fixture(scope='function')
async def chat_app_requester(guillotina):
    return chat_app_Requester(guillotina)
