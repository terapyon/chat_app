pytest_plugins = [
    'aiohttp.pytest_plugin',
    'guillotina.tests.fixtures',
    'chat_app.tests.fixtures'
]
