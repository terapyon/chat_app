import asyncio


async def test_install(chat_app_requester):  # noqa
    async with chat_app_requester as requester:
        response, _ = await requester('GET', '/db/guillotina/@addons')
        assert 'chat_app' in response['installed']
