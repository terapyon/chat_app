from guillotina import configure
from guillotina import schema
from guillotina import interfaces
from guillotina import content
from guillotina import Interface


class ITeam(Interface):
    text = schema.Text(required=False)


class IChanel(Interface):
    text = schema.Text(required=False)


class IMessage(Interface):
    text = schema.Text()


@configure.contenttype(
    type_name="Team",
    schema=ITeam,
    behaviors=[
        "guillotina.behaviors.dublincore.IDublinCore",
    ],
    allowed_types=['Chanel'],
    )
class Team(content.Folder):
    """
    Team Folder
    """


@configure.contenttype(
    type_name="Chanel",
    schema=IChanel,
    behaviors=[
        "guillotina.behaviors.dublincore.IDublinCore",
        "guillotina.behaviors.attachment.IAttachment"
    ],
    allowed_types=['Message'])
class Chanel(content.Folder):
    """
    Chanel Folder
    """


@configure.contenttype(
    type_name="Message",
    schema=IMessage,
    behaviors=[
        "guillotina.behaviors.dublincore.IDublinCore",
        "guillotina.behaviors.attachment.IAttachment"
    ],
    allowed_types=['Chanel'])
class Message(content.Item):
    """
    Message Item
    """

