from guillotina import configure


app_settings = {
    # provide custom application settings here...
}


def includeme(root):
    """
    custom application initialization here
    """
    configure.scan('chat_app.api')
    configure.scan('chat_app.install')
    configure.scan('chat_app.content')
    configure.scan('chat_app.services')
    configure.scan('chat_app.serialize')
